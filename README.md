# Analysis T1 - Zusammenfassung

Zusammenfassung aller Inhalte der Universitäts-Vorlesung Analysis 1.

# Fokus: 
Vor allem eine übersichtliche Zusammenfassung von Folgen & Reihen.

# Vorsicht: 
In etlichen anderen Aspekten noch unvollständig!

# Feedback:
Feature-Requests und gefundene Fehler einfach als Issues einreichen.
