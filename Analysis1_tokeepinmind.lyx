#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass book
\use_default_options false
\begin_modules
theorems-std
\end_modules
\maintain_unincluded_children false
\language ngerman
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family rmdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 95 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures false
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification false
\use_refstyle 0
\use_minted 0
\index Stichwortverzeichnis
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 2
\tocdepth 1
\paragraph_separation skip
\defskip medskip
\is_math_indent 0
\math_numbering_side default
\quotes_style german
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Analysis - Things to keep in Mind
\begin_inset Foot
status open

\begin_layout Plain Layout
Aktuelle Versionen online verfügbar unter: 
\begin_inset CommandInset href
LatexCommand href
name "https://oberaigner.info/studium"
target "https://oberaigner.info/studium"
literal "false"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Author
Max Oberaigner
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
renewcommand{
\backslash
chaptername}{Kapitel}
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Folgen und Reihen
\end_layout

\begin_layout Section
Beweis, dass eine Folge einen Grenzwert hat
\end_layout

\begin_layout Standard
Es gibt mehrere Möglichkeiten, hier sortiert nach 
\begin_inset Quotes gld
\end_inset

Nettigkeit
\begin_inset Quotes grd
\end_inset

.
\end_layout

\begin_layout Standard
(rek.) gibt im folgenden Methoden an, welche für rekursive
\begin_inset Foot
status open

\begin_layout Plain Layout
d.h.
 in der Form 
\begin_inset Formula $a_{n+1}=a_{n}+...$
\end_inset

 oder ähnlich.
\end_layout

\end_inset

  Folgen funktionieren können, (expl.) gibt Methoden an, welche für explizite
\begin_inset Foot
status open

\begin_layout Plain Layout
d.h.
 in der Form 
\begin_inset Formula $a_{n}=...$
\end_inset

 oder ähnlich.
\end_layout

\end_inset

 Folgen funktionieren können.
\end_layout

\begin_layout Subsubsection
Trick für rekursive Folgen:
\end_layout

\begin_layout Standard
Versuchen, sie in explizite Form zu bringen, diese ist leichter handhabbar.
\end_layout

\begin_layout Standard
Vorsicht: Hier nicht zu viel Zeit verplempern, oft ist dieser Schritt bei
 Aufgabenstellungen nicht vorgesehen und führt daher in die Irre.
\end_layout

\begin_layout Subsection
Monotonie + Beschränktheit (rek.
 & expl.)
\end_layout

\begin_layout Subsubsection
Monotoniebeweis
\end_layout

\begin_layout Standard
Zunächst erste Folgenglieder ansehen, um Verhalten abschätzen zu können.
\end_layout

\begin_layout Standard
Möglichkeiten:
\end_layout

\begin_layout Enumerate
Bei rekursiv gegebenen Folgen: Induktion
\end_layout

\begin_layout Enumerate
Ansonsten: Allgemein zeigen
\end_layout

\begin_layout Standard
Möglichkeiten:
\end_layout

\begin_layout Standard
\begin_inset Formula $a_{n+1}-a_{n}\leqslant0$
\end_inset

 bzw.
 
\begin_inset Formula $a_{n+1}-a_{n}\eqslantgtr0$
\end_inset


\end_layout

\begin_layout Subsubsection
Beschränktheitsbeweis (expl.)
\end_layout

\begin_layout Enumerate
Bei rekursiv gegebenen Folgen: Induktion
\end_layout

\begin_layout Enumerate
allgemein zeigen
\end_layout

\begin_layout Remark
Bei Summen mit größtem und kleinsten Element lässt sich die Schranke gut
 abschätzen:
\end_layout

\begin_layout Remark
Untere Grenze von 
\begin_inset Formula $\sum_{n=0}^{k}a_{n}$
\end_inset

 ist 
\begin_inset Formula $k\cdot min(a_{n})$
\end_inset

.
\end_layout

\begin_layout Remark
Obere Grenze von 
\begin_inset Formula $\sum_{n=0}^{k}a_{n}$
\end_inset

 ist 
\begin_inset Formula $k\cdot max(a_{n})$
\end_inset

.
\end_layout

\begin_layout Remark
Dieser Ausdruck lässt sich oft leicht so auflösen, dass Abhängigkeiten zu
 
\begin_inset Formula $k$
\end_inset

 verschwinden.
\end_layout

\begin_layout Remark
Anderenfalls muss man ihn geschickt nach oben bzw.
 unten abschätzen.
\end_layout

\begin_layout Standard
Somit muss, um eine Grenze zu finden, unter Umständen nur das größte und
 das kleinste Element der Summe gefunden werden können!
\end_layout

\begin_layout Subsection
Grenzwertrechenregeln (expl.)
\end_layout

\begin_layout Standard
Zerlegung des Ausdrucks in konvergierende Teilfolgen.
\end_layout

\begin_layout Standard
Der Grenzwert lässt sich mittels der Grenzwertrechenregeln ermitteln.
\end_layout

\begin_layout Standard
Bei Bruchausdrücken wie 
\begin_inset Formula $\frac{n²+5n+7}{3n²+8n-3}$
\end_inset

 durch - in diesem Fall 
\begin_inset Formula $n^{2}$
\end_inset

 - 
\begin_inset Quotes gld
\end_inset

kürzen
\begin_inset Quotes grd
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\Rightarrow\frac{1+\frac{5}{n}+\frac{7}{n\text{²}}}{3+\frac{8}{n}-\frac{3}{n\text{²}}}\right)$
\end_inset

 Dann mithilfe der Limesrechenregeln auflösen.
 
\begin_inset Formula $\left(\Rightarrow\frac{1}{3}\right)$
\end_inset


\end_layout

\begin_layout Standard
(
\begin_inset Quotes gld
\end_inset

Richtigen
\begin_inset Quotes grd
\end_inset

 Beweis u.U.
 auch noch nachreichen oder: Zeigen in die andere Richtung (Ausgehen von
 bekannten konvergenten Folgen und konstruieren der Folge daraus.)
\end_layout

\begin_layout Subsection
Einzwicken (
\begin_inset Quotes gld
\end_inset

rek.
\begin_inset Quotes grd
\end_inset

 & expl.)
\end_layout

\begin_layout Standard
Einzwicken der Folge zwischen zwei anderen.
\end_layout

\begin_layout Subsection
Cauchy-Kriterium (
\begin_inset Quotes gld
\end_inset

rek.
\begin_inset Quotes grd
\end_inset

 & expl.)
\end_layout

\begin_layout Standard
(Es sei 
\begin_inset Formula $n>N$
\end_inset

 und 
\begin_inset Formula $k\in\mathbb{N^{*}}$
\end_inset

)
\end_layout

\begin_layout Standard
Ansatz:
\end_layout

\begin_layout Standard
\begin_inset Formula $\left|a_{n+1}-a_{n+k+1}\right|<\varepsilon$
\end_inset


\end_layout

\begin_layout Standard
Dann umformen, bis:
\end_layout

\begin_layout Standard
\begin_inset Formula $\frac{blabla}{blablabla}\left|a_{n}-a_{n+k}\right|<\varepsilon$
\end_inset


\end_layout

\begin_layout Standard
und dann 
\begin_inset Formula $\frac{blabla}{blablabla}$
\end_inset

 so nach oben abschätzen, dass
\end_layout

\begin_layout Standard
\begin_inset Formula $\frac{1}{z}\left|a_{n}-a_{n+k}\right|<\varepsilon$
\end_inset

 und 
\begin_inset Formula $z>1$
\end_inset


\end_layout

\begin_layout Standard
dann - um Beweis zu vervollständigen: 
\begin_inset Quotes gld
\end_inset

Aufrollen
\begin_inset Quotes grd
\end_inset

:
\end_layout

\begin_layout Standard
\begin_inset Formula $\frac{1}{z}^{n}\left|a_{0}-a_{k}\right|<\varepsilon$
\end_inset


\end_layout

\begin_layout Subsection
Direktes Zeigen des Grenzwertes (expl.)
\end_layout

\begin_layout Standard
Zunächst Grenzwert 
\begin_inset Quotes gld
\end_inset

erraten
\begin_inset Quotes grd
\end_inset

.
\end_layout

\begin_layout Standard
Dann zeigen, dass
\end_layout

\begin_layout Standard
\begin_inset Formula $\left|a_{n}-a\right|<\varepsilon$
\end_inset


\end_layout

\begin_layout Standard
durch Einsetzen von 
\begin_inset Formula $a$
\end_inset

.
\end_layout

\begin_layout Standard
Jetzt umformen mit dem Ziel:
\end_layout

\begin_layout Standard
\begin_inset Formula $n>blaepsilonisthierdrinblub$
\end_inset


\end_layout

\begin_layout Standard
und damit ist der Beweis vollbracht.
\end_layout

\begin_layout Section
Gegenbeweise
\end_layout

\begin_layout Subsection
Unbeschränktheit (rek.
 & expl.)
\end_layout

\begin_layout Standard
Wenn eine Folge nicht beschränkt ist, so kann sie auch nicht konvergieren.
\end_layout

\begin_layout Subsection
divergente Teilfolge (rek.
 & expl.)
\end_layout

\begin_layout Standard
Divergiert eine Teilfolge der betrachteten Folge, so divergiert auch die
 Folge selbst.
\end_layout

\begin_layout Section
Beweis eines Häufungspunktes
\end_layout

\begin_layout Subsubsection
Vorhandensein:
\end_layout

\begin_layout Standard
Jede beschränkte Folge hat mindestens einen Häufungspunkt.
\end_layout

\begin_layout Subsubsection
Konkretes Auffinden:
\end_layout

\begin_layout Enumerate
Jeder Grenzwert ist ein Häufungspunkt.
 (Ist dieser Häufungspunkt ev.
 ein Grenzwert?)
\end_layout

\begin_layout Enumerate
Einfach zeigen, dass eine Teilfolge der betrachteten Folge den gesuchten
 Häufungspunkt als Grenzwert hat.
 (siehe oben)
\end_layout

\begin_layout Section
Beweis, dass eine Reihe einen Grenzwert hat
\end_layout

\begin_layout Standard
Es gibt mehrere Möglichkeiten, wieder sortiert nach 
\begin_inset Quotes gld
\end_inset

Nettigkeit
\begin_inset Quotes grd
\end_inset

.
 Weiters sind mit 
\begin_inset Quotes gld
\end_inset

ev.
\begin_inset Quotes grd
\end_inset

 markierte Vorgehensweisen nur in manchen Fällen möglich (wie jeweils angemerkt).
\end_layout

\begin_layout Subsubsection
Tipp: 
\begin_inset Quotes gld
\end_inset

Teleskopsummen
\begin_inset Quotes grd
\end_inset


\end_layout

\begin_layout Standard
Die betreffende Reihe zunächst ansehen und herausfinden, ob sich verschiedene
 Elemente gegenseitig auslöschen.
 Dies kann die Arbeit erheblich erleichtern oder sogar das Summenzeichen
 völlig entfernen.
\end_layout

\begin_layout Standard
Beachte: ev.
 Partialbruchzerlegung nötig.
 (siehe Kapitel 
\begin_inset Quotes gld
\end_inset

allgemeine Techniken
\begin_inset Quotes grd
\end_inset

)
\end_layout

\begin_layout Subsubsection
Tipp: Absolutwerte
\end_layout

\begin_layout Standard
Wenn eine Reihe des Absolutwerts einer Folge einen Grenzwert hat, dann hat
 auch die Reihe der selben Folge ohne Absolutwert einen Grenzwert.
\end_layout

\begin_layout Standard
\begin_inset Formula $\sum_{k=1}^{\infty}\left|a_{k}\right|\rightarrow a''\Longrightarrow\sum_{k=1}^{\infty}a_{k}\rightarrow a'$
\end_inset


\end_layout

\begin_layout Subsection
ev.
 alternierende Reihen / Leibnizkriterium
\end_layout

\begin_layout Standard
Nur bei alternierenden Reihen (mit abwechselnd positiven und negativen Summanden
) anwendbar.
\end_layout

\begin_layout Standard
\begin_inset Quotes gld
\end_inset

Eine alternierende Reihe konvergiert, wenn die Folge der Summanden eine
 Nullfolge ist.
\begin_inset Quotes grd
\end_inset


\end_layout

\begin_layout Paragraph
Anwendung
\end_layout

\begin_layout Enumerate
Beweis des Alternierens, ev.
 durch 
\begin_inset Formula $a_{n}\cdot a_{n+1}<0$
\end_inset


\end_layout

\begin_layout Enumerate
Zeige, dass 
\begin_inset Formula $\underset{n\rightarrow\infty}{lim}a_{n}=0$
\end_inset


\end_layout

\begin_layout Standard
Damit ist die Konvergenz bewiesen.
\end_layout

\begin_layout Subsection
Geometrische Reihe finden
\end_layout

\begin_layout Standard
\begin_inset Quotes gld
\end_inset

Wenn 
\begin_inset Formula $0\leq a_{n}\leq q^{n}$
\end_inset

 und 
\begin_inset Formula $\left|q\right|<1$
\end_inset

, so ist die Reihe konvergent.
\begin_inset Quotes grd
\end_inset


\end_layout

\begin_layout Paragraph
Anwendung:
\end_layout

\begin_layout Subparagraph
Variante A (
\begin_inset Quotes gld
\end_inset

die Schöne
\begin_inset Quotes grd
\end_inset

):
\end_layout

\begin_layout Standard
Vorsicht: Oft nicht möglich!
\end_layout

\begin_layout Enumerate
Zeigen: 
\begin_inset Formula $a_{n}\geq0$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}$
\end_inset

 so umformen, dass sie dem Ausdruck 
\begin_inset Formula $q^{n}$
\end_inset

 entspricht.
\end_layout

\begin_layout Enumerate
Überprüfen ob 
\begin_inset Formula $q<1$
\end_inset


\end_layout

\begin_layout Subparagraph
Variante B (
\begin_inset Quotes gld
\end_inset

die Nützlichere
\begin_inset Quotes grd
\end_inset

):
\end_layout

\begin_layout Standard
Oft universeller einsetzbar.
\end_layout

\begin_layout Enumerate
Zeigen: 
\begin_inset Formula $a_{n}\geq0$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}$
\end_inset

 so nach oben abschätzen, dass sich ein Ausdruck 
\begin_inset Formula $q^{n}$
\end_inset

 gilt.
\end_layout

\begin_layout Enumerate
Überprüfen ob 
\begin_inset Formula $q<1$
\end_inset


\end_layout

\begin_layout Subsection
ev.
 Quotientenregel
\end_layout

\begin_layout Standard
Nur sinnvoll anwendbar, wenn sich aufeinanderfolgende Folgenglieder schön
 kürzen.
\end_layout

\begin_layout Standard
\begin_inset Quotes gld
\end_inset

Wenn 
\begin_inset Formula $\left|\frac{a_{k+1}}{a_{k}}\right|<1\:\forall k\in\mathbb{N}^{*}$
\end_inset

, dann ist die Reihe konvergent.
\begin_inset Quotes grd
\end_inset


\end_layout

\begin_layout Paragraph
Anwendung
\end_layout

\begin_layout Enumerate
Zeige 
\begin_inset Formula $limsup\left(\left|\frac{a_{k+1}}{a_{k}}\right|\right)<1$
\end_inset

, damit ist die Konvergenz bewiesen.
\end_layout

\begin_layout Enumerate
Falls 
\begin_inset Formula $limsup\left(\left|\frac{a_{k+1}}{a_{k}}\right|\right)>1$
\end_inset

 sein sollte, divergiert die Reihe.
\end_layout

\begin_layout Subsection
ev.
 Wurzelregel
\end_layout

\begin_layout Standard
Nur bei bestimmten Potenzausdrücken sinnvoll anwendbar.
\end_layout

\begin_layout Standard
Falls die Quotientenregel fehlgeschlagen ist, macht Wurzelregel auch keinen
 Sinn.
\end_layout

\begin_layout Standard
\begin_inset Quotes gld
\end_inset

Wenn 
\begin_inset Formula $\sqrt[k]{\left|a_{k}\right|}<1\:\forall k\in\mathbb{N}^{*}$
\end_inset

, dann ist die Reihe konvergent.
\begin_inset Quotes grd
\end_inset


\end_layout

\begin_layout Paragraph
Anwendung
\end_layout

\begin_layout Enumerate
Zeige 
\begin_inset Formula $limsup\left(\sqrt[k]{\left|a_{k}\right|}\right)<1$
\end_inset

, damit ist die Konvergenz bewiesen.
\end_layout

\begin_layout Enumerate
Falls 
\begin_inset Formula $limsup\left(\sqrt[k]{\left|a_{k}\right|}\right)>1$
\end_inset

 sein sollte, divergiert die Reihe.
\end_layout

\begin_layout Subsection
ev.
 Verdichtung
\end_layout

\begin_layout Standard
\begin_inset Quotes gld
\end_inset


\begin_inset Formula $\sum_{k=0}^{\infty}2^{k}a_{2^{k}}$
\end_inset

 konvergiert genau dann, wenn 
\begin_inset Formula $\sum_{n=0}^{\infty}a_{n}$
\end_inset

 konvergiert.
\begin_inset Quotes grd
\end_inset


\end_layout

\begin_layout Standard
Dies kann in einigen wenigen Fällen zu einer Vereinfachung führen.
\end_layout

\begin_layout Paragraph
Anwendung:
\end_layout

\begin_layout Enumerate
Prüfen ob sinnvoll: Kürzt sich 
\begin_inset Formula $2^{k}$
\end_inset

 wieder weg?
\end_layout

\begin_layout Enumerate
Einsetzen in die Ursprungsfolge
\end_layout

\begin_layout Enumerate
Vereinfachen und kürzen
\end_layout

\begin_layout Enumerate
(ev.
 in Form einer geometrischen Reihe bringen)
\end_layout

\begin_layout Enumerate
Eine der anderen Regeln anwenden (z.B.: die für geometrische Reihen), um Konvergen
z der sich ergebenden Folge zu beweisen.
\end_layout

\begin_layout Subsection
Majorantenkriterium
\end_layout

\begin_layout Standard
\begin_inset Quotes gld
\end_inset

Für Reihen mit 
\begin_inset Formula $a_{n}\geq0$
\end_inset

 (
\begin_inset Formula $\Leftrightarrow$
\end_inset

 monoton steigend) gilt: Sind alle Summenglieder/Partialsummen einer Reihe
 kleiner als jene einer anderen, konvergierenden Reihe, so konvergiert auch
 diese Reihe.
\begin_inset Quotes grd
\end_inset


\end_layout

\begin_layout Standard
Hierzu ist es wichtig so viele konvergente Reihen wie möglich zu kennen.
\end_layout

\begin_layout Subsection
Cauchy-Kriterium für Reihen
\end_layout

\begin_layout Standard
Eine Reihe konvergiert dann, wenn 
\begin_inset Formula $\left|\sum_{k=n+1}^{n+l}a_{k}\right|<\varepsilon$
\end_inset

, also der verbleibende 
\begin_inset Quotes gld
\end_inset

Rest
\begin_inset Quotes grd
\end_inset

 einer Reihe für ausreichend große 
\begin_inset Formula $N$
\end_inset

 mit 
\begin_inset Formula $n>N$
\end_inset

 beliebig klein wird.
 
\begin_inset Formula $l$
\end_inset

 ist hierbei eine beliebige natürliche Zahl.
\end_layout

\begin_layout Subsection
'Monotonie'+ Beschränktheit
\end_layout

\begin_layout Standard
Für Reihen mit 
\begin_inset Formula $a_{n}\geq0$
\end_inset

 (
\begin_inset Formula $\Leftrightarrow$
\end_inset

 monoton steigend) gilt:
\end_layout

\begin_layout Standard
Ist die Folge der Partialsummen einer Reihe beschränkt, so ist die Reihe
 konvergent.
\end_layout

\begin_layout Paragraph
Vorgehensweise:
\end_layout

\begin_layout Enumerate
Zeigen, dass 
\begin_inset Formula $a_{n}\geq0$
\end_inset


\end_layout

\begin_layout Enumerate
Beweis der Beschränktheit von 
\begin_inset Formula $\underset{k\rightarrow\infty}{lim}\left(\sum_{n=0}^{k}a_{n}\right)$
\end_inset


\end_layout

\begin_layout Section
Gegenbeweis
\end_layout

\begin_layout Subsection
Trivialregel - Nullfolge
\end_layout

\begin_layout Standard
Kann nicht konvergieren, wenn
\begin_inset Formula $\underset{n\rightarrow\infty}{lim}\left(a_{k}\right)\neq0$
\end_inset

, wobei 
\begin_inset Formula $a_{k}$
\end_inset

 ein Summand der Reihe ist.
\end_layout

\begin_layout Subsection
Minorantenregel
\end_layout

\begin_layout Standard
Sind alle Summenglieder/Partialsummen einer Reihe größer als jene einer
 anderen, divergierenden Reihe, so divergiert auch diese Reihe.
\end_layout

\begin_layout Standard
Hierzu ist es wichtig so viele divergente Reihen wie möglich zu kennen.
\end_layout

\begin_layout Section
Berechnen des Grenzwerts einer Reihe
\end_layout

\begin_layout Standard
Allgemein eher schwierig, im folgenden einige Methoden, die helfen können:
\end_layout

\begin_layout Subsection
Sichten der Summe nach Mustern
\end_layout

\begin_layout Standard
Tipp: Ist nichts sichtbar: Abziehen des (vermuteten) Grenzwerts, so wird
 Muster teils besser erkennbar.
\end_layout

\begin_layout Enumerate
Sichten der Folge der Teilsumme
\end_layout

\begin_layout Enumerate
Aufstellen einer expliziten Darstellung dieser Folge
\end_layout

\begin_layout Enumerate
Beweis der Äquivalenz durch Induktion
\end_layout

\begin_layout Enumerate
Grenzwert der gefundenen Folge berechnen
\end_layout

\begin_layout Subsection
Aufteilen in Partialbrüche
\end_layout

\begin_layout Enumerate
Teleskopsummen suchen (Die Partialbrüche für die ersten und die letzten
 paar Glieder hinschreiben)
\end_layout

\begin_layout Enumerate
Aufstellen einer expliziten Darstellung dieser Folge
\end_layout

\begin_layout Enumerate
Beweis der Äquivalenz durch Induktion
\end_layout

\begin_layout Enumerate
Grenzwert der gefundenen Folge berechnen
\end_layout

\begin_layout Subsection
Äquivalenz mit Reihen bekannten Grenzwerts zeigen
\end_layout

\begin_layout Enumerate
zu 
\begin_inset Formula $\sum q^{n}$
\end_inset

 (geometr.
 Reihe)
\end_layout

\begin_layout Enumerate
Grenzwert mit 
\begin_inset Formula $\frac{1}{1-q}$
\end_inset

 berechnen.
\end_layout

\begin_layout Standard
oder
\end_layout

\begin_layout Standard
allgemein: Es bieten sich alle Reihen bekannten Grenzwerts an.
\end_layout

\begin_layout Subsection
Einzwicken mit anderen Summen desselben Grenzwerts
\end_layout

\begin_layout Chapter
allgemeine Techniken
\end_layout

\begin_layout Section
Partialbruchzerlegung
\end_layout

\begin_layout Standard
z.B: 
\begin_inset Formula $\frac{2x-4}{x^{2}-2x-8}$
\end_inset


\end_layout

\begin_layout Standard
Zunächst den Nenner in Linearfaktorzerlegung schreiben.
 Hierzu Nullstellen bestimmen und dann Satz von Vieta anwenden.
\end_layout

\begin_layout Standard
z.B: Nullstellen: 
\begin_inset Formula $x_{1}=4$
\end_inset

; 
\begin_inset Formula $x_{2}=-2$
\end_inset

, daher: 
\begin_inset Formula $\frac{2x-4}{(x-4)(x+2)}$
\end_inset


\end_layout

\begin_layout Standard
Dann 
\begin_inset Quotes gld
\end_inset

Auseinanderziehen
\begin_inset Quotes grd
\end_inset

.
\end_layout

\begin_layout Standard
z.B: 
\begin_inset Formula $\frac{a}{(x-4)}+\frac{b}{(x+2)}$
\end_inset

 (beachte: neue Variablen 
\begin_inset Formula $a,b$
\end_inset

, welche erst bestimmt werden müssen)
\end_layout

\begin_layout Standard
Bestimmen von 
\begin_inset Formula $a$
\end_inset

 und 
\begin_inset Formula $b$
\end_inset

:
\end_layout

\begin_layout Enumerate
Wieder-auf-einen-Nenner-bringen
\end_layout

\begin_layout Enumerate
Zähler ausmultiplizieren
\end_layout

\begin_layout Enumerate
\begin_inset Formula $x$
\end_inset

 im Zähler herausheben
\end_layout

\begin_layout Enumerate
Vergleiche: Zähler des neuen Bruchs und Zähler des alten Bruchs: Aufstellen
 eines Gleichungssystems für 
\begin_inset Formula $a$
\end_inset

 und 
\begin_inset Formula $b$
\end_inset

.
\end_layout

\begin_layout Enumerate
Lösen dieses Gleichungssystems
\end_layout

\begin_layout Standard
z.B: 
\begin_inset Formula $\frac{a(x+2)+b(x-4)}{(x-4)(x+2)}=\frac{ax+2a+bx-4b}{(x-4)(x+2)}=\frac{(a+b)x+2a-4b}{(x-4)(x+2)}$
\end_inset

, dann: 
\begin_inset Formula $(a+b)x+2a-4b=2x-4$
\end_inset

 somit gilt das Gleichungssystem: 
\begin_inset Formula $\begin{array}{c}
a+b=2\\
2a-4b=-4
\end{array}$
\end_inset

, dies lösen, somit: 
\begin_inset Formula $\begin{array}{c}
a=\frac{2}{3}\\
b=\frac{4}{3}
\end{array}$
\end_inset


\end_layout

\begin_layout Standard
Einsetzen von 
\begin_inset Formula $a$
\end_inset

 und 
\begin_inset Formula $b$
\end_inset

 in die Brüche, die aus dem Schritt 
\begin_inset Quotes gld
\end_inset

Auseinanderziehen
\begin_inset Quotes grd
\end_inset

 entstanden sind.
\end_layout

\begin_layout Standard
z.B: 
\begin_inset Formula $\frac{\frac{2}{3}}{x-4}+\frac{\frac{4}{3}}{x+2}$
\end_inset


\end_layout

\begin_layout Standard
Dies ist das Ergebnis der Partialbruchzerlegung.
\end_layout

\end_body
\end_document
